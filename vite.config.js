import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
// 查看打包后文件分析
import { visualizer } from "rollup-plugin-visualizer";
// 自动打包配置
import AutoImport from 'unplugin-auto-import/vite'; 
import Components from "unplugin-vue-components/vite";
import { NaiveUiResolver } from "unplugin-vue-components/resolvers";
import VueSetuoExtend from 'vite-plugin-vue-setup-extend';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(), 
        visualizer({
            emitFile: true,
            filename: "analysis.html",
            title: "项目依赖可视化分析报告",
            sourcemap: false,
            open: false,
        }),
        AutoImport({
            imports: ["vue", "vue-router",{
                'naive-ui': [
                  'useDialog',
                  'useMessage',
                  'useNotification',
                  'useLoadingBar'
                ]
            }]
        }),
        Components({
            // relative paths to the directory to search for components.
            dirs: ['src/components'],
            // Allow for components to override other components with the same name
            allowOverrides: false,
            resolvers: [NaiveUiResolver()]
        }),
        VueSetuoExtend()
    ],
    server: {
        host: 'localhost',
        port: '1024',
        hmr: true
    },
    resolve:{
        alias: {
            '@': path.resolve(__dirname, './src')
        }
    },
    css: {
        preprocessorOptions: {
            scss: {
                additionalData: `@import "@/assets/styles/global.scss";`
            }
        }
    },
    build:{ // 分块打包配置
        chunkSizeWarningLimit: 1500, // 分块打包，分解块，将大块分解成更小的块
        rollupOptions: {
            output:{
                manualChunks(id) {
                    if (id.includes('node_modules')) {
                        return id.toString().split('node_modules/')[1].split('/')[0].toString();
                    }
                }
            }
        }
    }
})
