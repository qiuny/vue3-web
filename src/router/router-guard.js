import { isEnableDynamicRouter, projectNameCn } from '../scripts/global'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import router from './index'

NProgress.configure({
    showSpinner: false,
})

/**
 * 全局前置守卫函数
 * 
 * @param { Object } to 
 * @param { Object } from 
 */
export const beforeNavRouteGuard = (to, from) => {
    NProgress.start()
    if (isEnableDynamicRouter) {
        // 开启动态菜单处理，根据权限合并路由
    } else {
        // 否则默认跳转目标路由
        return true
    }
}

/**
 * 全局后置守卫函数
 * 
 * @param { Object } to 
 * @param { Object } from 
 */
export const afterNavRouteGuard = (to, from) => {
    const { query } = to
    document.title = `${query.title || to.meta.title}-${projectNameCn}` 
    NProgress.done()
    // router.addRoute({
    //     path: '/test',
    //     name: '测试',
    //     component: () => import('@/views/test/index.vue'),
    //     meta: { title: '测试路由', hidden: true }
    // })
    // router.getRoutes()
}