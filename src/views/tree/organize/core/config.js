export const initCofing = {
    intervalX: 150, // 水平间距，也是层级间距
    intervalY: 100, // 垂直间距，也是相邻节点间距
    padding: 20, // 相邻节点补白
    childX: 0, // 每个子节点的x坐标
    childY: 0, // 每个子节点的y坐标
    startY: 0,  // 所有孩子节点区域的y坐标，也就是区域高度起点
    startX: 0, // 所有孩子节点区域的左边x坐标，也就是区域宽度起点
    childrenAreaHeight:0, // 子节点总区域高度
    childrenAreaWidth:0, // 子节点总区域宽度
    rankdir: 'LR', // 默认LR，支持TB(从上到下布局)和LR(从左往右布局)
    color: {
        stroke: '#18a058',
        background: '#e7f5ee',
        textColor: '#18a058'
    },
    color1: {
        stroke: '#E68369',
        background: '#FBF6E2',
        textColor: '#131842'
    }
}