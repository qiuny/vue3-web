import Raphael from 'raphael'
import { createShape } from './shapes'

// 自定义Raphael方法，可通过Raphael直接调用
Raphael.fn.connection = function (obj1, obj2, line, bg) {
    // console.log(obj1, obj2, line, bg, 788);
    if (obj1.line && obj1.from && obj1.to) {
        line = obj1;
        obj1 = line.from;
        obj2 = line.to;
    }
    let bb1 = obj1.getBBox(),
        bb2 = obj2.getBBox(),
        p = [{x: bb1.x + bb1.width / 2, y: bb1.y - 1},
        {x: bb1.x + bb1.width / 2, y: bb1.y + bb1.height + 1},
        {x: bb1.x - 1, y: bb1.y + bb1.height / 2},
        {x: bb1.x + bb1.width + 1, y: bb1.y + bb1.height / 2},
        {x: bb2.x + bb2.width / 2, y: bb2.y - 1},
        {x: bb2.x + bb2.width / 2, y: bb2.y + bb2.height + 1},
        {x: bb2.x - 1, y: bb2.y + bb2.height / 2},
        {x: bb2.x + bb2.width + 1, y: bb2.y + bb2.height / 2}],
        d = {}, dis = [];
    for (let i = 0; i < 4; i++) {
        for (let j = 4; j < 8; j++) {
            var dx = Math.abs(p[i].x - p[j].x),
                dy = Math.abs(p[i].y - p[j].y);
            if ((i == j - 4) || (((i != 3 && j != 6) || p[i].x < p[j].x) && ((i != 2 && j != 7) || p[i].x > p[j].x) && ((i != 0 && j != 5) || p[i].y > p[j].y) && ((i != 1 && j != 4) || p[i].y < p[j].y))) {
                dis.push(dx + dy);
                d[dis[dis.length - 1]] = [i, j];
            }
        }
    }
    if (dis.length == 0) {
        var res = [0, 4];
    } else {
        res = d[Math.min.apply(Math, dis)];
    }
    var x1 = p[res[0]].x,
        y1 = p[res[0]].y,
        x4 = p[res[1]].x,
        y4 = p[res[1]].y;
    dx = Math.max(Math.abs(x1 - x4) / 2, 10);
    dy = Math.max(Math.abs(y1 - y4) / 2, 10);
    var x2 = [x1, x1, x1 - dx, x1 + dx][res[0]].toFixed(3),
        y2 = [y1 - dy, y1 + dy, y1, y1][res[0]].toFixed(3),
        x3 = [0, 0, 0, 0, x4, x4, x4 - dx, x4 + dx][res[1]].toFixed(3),
        y3 = [0, 0, 0, 0, y1 + dy, y1 - dy, y4, y4][res[1]].toFixed(3);
    var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", x2, y2, x3, y3, x4.toFixed(3), y4.toFixed(3)].join(",");
    if (line && line.line) {
        line.bg && line.bg.attr({path: path});
        line.line.attr({path: path});
    } else {
        var color = typeof line == "string" ? line : "#000";
        return {
            bg: bg && bg.split && this.path(path).attr({stroke: bg.split("|")[0], fill: "none", "stroke-width": bg.split("|")[1] || 3}),
            line: this.path(path).attr({stroke: color, fill: "none"}),
            from: obj1,
            to: obj2
        };
    }
}
let connections = [], R = null;
export const initRaphaelGraph = ({ container = '' }) => {
    const containerElement = document.querySelector(container)
    const { clientWidth, clientHeight } = containerElement;
    // 构建svg容器
    R = new Raphael(containerElement, clientWidth, clientHeight);
    const svgDom = document.querySelector('svg') || null;
    const shapesList = createShape(R)
    let i = 0;
    while (i < shapesList.length) {
        const randomColor = Raphael.getColor()
        const element = shapesList[i]
        // 随机生成颜色
        element.attr({ fill: randomColor, stroke: randomColor, "fill-opacity": 0.1, "stroke-width": 2, cursor: "move" })
        // if (element.type === 'rect') {
        //     element.attr('fill', "#18a058")
        //     element.attr('cursor', "pointer")
        // }
        // if (element.type === 'circle') {
        //     element.attr("cursor", 'pointer')
        //     element.attr('fill', "tomato")
        //     element.attr('stroke', 'none')
        // }

        // 绑定拖拽事件
        element.drag(onDragMove, onDragStart, onDragEnd)

        // 绑定hover事件
        // element.hover(() => {
        //     element.attr('stroke', '#2d8cf0')
        //     element.attr('stroke-width', 2)
        // })

        // 绑定点击事件
        element.click(() => {
            // element.animate({ 'fill': 'pink'}, 500)
            // if (element.type == 'rect') element.animate({ 'x': element.attr('x')+18 }, 500)
            // if (element.type == 'rect') element.hide()
            // console.log(element, 777);
        })
        i++;
    }

    // 添加连线
    connections.push(R.connection(shapesList[0], shapesList[1], "#fff", "orange"))
    connections.push(R.connection(shapesList[0], shapesList[2], "#fff", "#10ac84"))

    // 设置svg视图
    R.setViewBox(0, 0, clientWidth, clientHeight)
    return R
}
function onDragMove(dx,dy, x, y, element) {
    const newAttr = this.type == "rect" ? { x: this.ox + dx, y: this.oy + dy } : { cx: this.ox + dx, cy: this.oy + dy};
    this.attr(newAttr);
    for(let j = connections.length; j--;) {
        R.connection(connections[j])
    }
    // Raphael 2.0 版本才有，而当前2.3版本不在挂载safari到Raphael实例
    // R.safari()
    // console.log(R, this, 66666);
    // console.log( 'onMove', this);
}
function onDragStart(dx,dy, x, y, element) {
    // const box = rectangle.getBBox()
    this.ox = this.type === 'rect' ? this.attr('x') : this.attr('cx')
    this.oy = this.type === 'rect' ? this.attr('y') : this.attr('cy')
    this.animate({ "fill-opacity": 0.5 }, 500)
    // console.log( 'onStart', this);
}
function onDragEnd(event) {
    event.preventDefault()
    // 当前对象this，就是拖拽对象,this中包括拖拽对象所有信息，包括原生事件，attr，node，papger，type
    this.animate({ "fill-opacity": 0.1 }, 500)
    // console.log('onEnd', this);
}