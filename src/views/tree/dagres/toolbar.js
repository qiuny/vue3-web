export const toolbar = [
    { type: 'download',text: '下载思维导图', icon: 'icon iconxiazai' },
    { type: 'closeFull',text: '关闭全屏', icon: 'icon iconguanbiquanping' },
    { type: 'openFull',text: '全屏查看', icon: 'icon iconquanping' },
    { type: 'RL',text: '右左布局', icon: 'icon icona-cengjijiagoubuju' },
    { type: 'LR',text: '左右布局', icon: 'icon icona-cengjijiagoubuju' },
    { type: 'TB',text: '上下布局', icon: 'icon icona-cengjijiagoubuju' },
    { type: 'BT',text: '下上布局', icon: 'icon icona-cengjijiagoubuju' },
]