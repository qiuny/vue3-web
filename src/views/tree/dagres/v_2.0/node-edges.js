const position = { x: 0, y: 0 };
const edgeType = "smoothstep";

export const initialNodes = [
    {
        id: "1",
        type: "input",
        data: { label: "input" },
        position
    },
    {
        id: "b1",
        data: { label: "node 2" },
        position
    },
    {
        id: "b2",
        data: { label: "node 2a" },
        position
    },
    {
        id: "c1",
        data: { label: "node 2b" },
        position
    },
    {
        id: "c2",
        data: { label: "node 2c" },
        position
    },
    {
        id: "c3",
        data: { label: "node 2d" },
        position
    }
    // {
    //   id: "d1",
    //   data: { label: "node 3" },
    //   position
    // },
    // {
    //   id: "d2",
    //   data: { label: "node 3" },
    //   position
    // },
    // {
    //   id: "d3",
    //   data: { label: "node 3" },
    //   position
    // },
    // {
    //   id: "d4",
    //   data: { label: "node 3" },
    //   position
    // },
    // {
    //   id: "d5",
    //   data: { label: "node 3" },
    //   position
    // },
    // {
    //   id: "d6",
    //   data: { label: "node 3" },
    //   position
    // },
    // {
    //   id: "d7",
    //   data: { label: "node 3" },
    //   position
    // },
    // {
    //   id: "d8",
    //   data: { label: "node 3" },
    //   position
    // }
];

export const initialEdges = [
    { id: "e12", source: "1", target: "b1", type: edgeType, animated: true },
    { id: "e13", source: "1", target: "b2", type: edgeType, animated: true },
    { id: "e22a", source: "b1", target: "c1", type: edgeType, animated: true },
    { id: "e22b", source: "b1", target: "c2", type: edgeType, animated: true },
    { id: "e22c", source: "b1", target: "c3", type: edgeType, animated: true },
    { id: "e22d", source: "b2", target: "c1", type: edgeType, animated: true },
    { id: "e22e", source: "b2", target: "c2", type: edgeType, animated: true },
    { id: "e22f", source: "b2", target: "c3", type: edgeType, animated: true },
    { id: "e45", source: "c1", target: "d1", type: edgeType, animated: true },
    { id: "e451", source: "c1", target: "d2", type: edgeType, animated: true },
    { id: "e452", source: "c1", target: "d3", type: edgeType, animated: true },
    { id: "e453", source: "c1", target: "d4", type: edgeType, animated: true },
    { id: "e4547", source: "c1", target: "d5", type: edgeType, animated: true },
    { id: "e4546", source: "c1", target: "d5", type: edgeType, animated: true },
    { id: "e4545", source: "c2", target: "d6", type: edgeType, animated: true },
    { id: "e4545", source: "c2", target: "d7", type: edgeType, animated: true },
    { id: "e4544", source: "c2", target: "d8", type: edgeType, animated: true },
    { id: "e4543", source: "c3", target: "d6", type: edgeType, animated: true },
    { id: "e4542", source: "c3", target: "d7", type: edgeType, animated: true },
    { id: "e4541", source: "c3", target: "d8", type: edgeType, animated: true }
];
