export default {
    rankdir: 'TB', // 节点布局方式：RL,LR,TB(默认值),BT
    nodesep: 50, // 节点之间的垂直间距，默认值50
    ranksep: 60, // 节点之间的水平间距，默认值60
    align: undefined, // 根节点对齐方式 DL,DR,UL,UR，默认值 undefined
    edgesep: 10, // 布局中水平分隔边缘的像素数
    marginx: 10,  // x 轴方向的边距
    marginy: 10,   // y 轴方向的边距
    ranker: 'network-simplex',
    directed: true, // :设置为true时,得到一个有向图。false时,得到一个无向图,默认有向图
    multigraph: false, // 设置为true时,允许图像在同一对节点之间有多条边,默认: false
    compound: false, // 设置为true时,允许图像有复合节点。可以是其他节点的父节点,默认为false
    nodes: [],
    links: []
}