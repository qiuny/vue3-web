import dagre from "@dagrejs/dagre";
import Raphael from 'raphael';
import svgPanZoom from 'svg-pan-zoom';
import { useWindowSize } from "@vueuse/core";
import { linkConnector } from './link';
import configuration from './configuration';
import { addition, subtraction } from './mark-sign';

export class BruceMindMap {
    constructor(
        el = '', 
        options = {
            rankdir: 'TB',
            align: undefined, 
            nodesep: 50,
            edgesep: 10,
            ranksep: 50,
            marginx: 0,
            marginy: 0,
            ranker: 'network-simplex',
            directed: true,
            multigraph: false,
            compound: false,
            nodes: [],
            links: [],
            edge:{
                fill: 'tomato',
                connector: 'curve'
            },
            viewPort: {
                zoom: 0.6
            }
        }
    ) {
        this.el = el
        this.config = Object.assign(configuration, options)
        this.graph = this.graph = new dagre.graphlib.Graph({ 
            directed: this.config.directed, 
            multigraph: this.config.multigraph, 
            compound: this.config.compound 
        })
        this.paper = null
        this.canvas = {}
        this.panZoomTiger = {}
        this.initCanvas()
        this.initGraph()
        this.initRaphael()
        this.initSvgPanZoom()
    }

    // 绘制画布
    initCanvas () {
        try {
            this.canvas = typeof this.el === 'string' ? document.querySelector(`${this.el}`) : this.el
        } catch (error) {
            throw new Error(`The container id${ this.el } is invalid or the page does not have a corresponding id container.`)
        }
    }
    
    // 初始化图形
    initGraph () {
        const { clientWidth, clientHeight } = this.canvas;
        // set Graph
        this.graph.setGraph({
            rankdir: this.config.rankdir,
            width: clientWidth,
            height: clientHeight,
            align: this.config.align,
            nodesep: this.config.nodesep,
            edgesep: this.config.edgesep, 
            ranksep: this.config.ranksep,
            marginx: this.config.marginx,
            marginy: this.config.marginy,
            ranker: this.config.ranker
        })
        // 1 默认为每个新边分配一个新对象作为标签。
        this.graph.setDefaultEdgeLabel(function() { return {}})
        // 2 添加节点到图形容器，第一个参数是节点ID，第二个有关节点的元数据，此处为每个节点添加标签
        // {...item.node, id: item.id } 属性会在 this.graph.nodes()中出现
        this.config.nodes.forEach(item => this.graph.setNode(item.id, {...item.node, id: item.id }))
        // 3 给图形容器添加边
        // { sourceId: edge.sourceId, targetId:edge.targetId } 属性会在this.graph.edges()中出现
        this.config.links.forEach(edge => this.graph.setEdge(edge.sourceId, edge.targetId, { sourceId: edge.sourceId, targetId:edge.targetId }))
    }
    // 渲染数据到页面
    initRaphael () {
        this.paper = null;
        const { clientWidth, clientHeight } = this.canvas;    
        this.updateLayout()
        this.paper = new Raphael(this.canvas, clientWidth, clientHeight)
        this.updateRender()
        // console.log(this.graph, this.getNodes(), clientWidth, clientHeight);
    }
    // 给svg绑定预览辅助工具
    initSvgPanZoom() {
        const svgElement = this.canvas.querySelector(`${this.el} > svg`)
        this.panZoomTiger = svgPanZoom(svgElement, {
            viewportSelector: `${this.el} > svg`,
            panEnabled: true, // 启用拖拽
            zoomEnabled: true, // 开启缩放
            controlIconsEnabled: true, // 开启图标控制，点击图标缩放
            dblClickZoomEnabled: false, // 禁止双击鼠标进行缩放视图
            mouseWheelZoomEnabled: true, // 开启鼠标滚轮缩放视图
            zoomScaleSensitivity: 0.2, // 变焦灵敏度 (Default 0.2)
            minZoom: 0.5, // 最小缩放级别 (Default 0.5)
            maxZoom: 10, // 最大缩放级别  (Default 10)
            fit: true, // 启用或禁用 SVG 中的视口适配
            center: true, // 在 SVG 中启用或禁用视口居中
            refreshRate: 10
        })
        // 默认以1倍查看视图，可以放大当前视图倍数，以获得更加的视觉效果
        this.panZoomTiger.zoom(this.config.viewPort.zoom)
        // 配置最小倍数
        // console.log(this.viewPort, 'initSvgPanZoom');
    }

   // 重新设置svg的视图属性，让内容居中显示
    resetViewBox = () => {
        const { clientWidth, clientHeight } = this.canvas; 
        if (this.graph) {
            // DagreJS图形的宽度和高度
            const graphWidth = this.graph.graph().width;
            const graphHeight = this.graph.graph().height;

            // 计算位移量
            const dx = (clientWidth - graphWidth) / 2;
            const dy = (clientHeight - graphHeight) / 2;

            // 设置viewBox保证内容完全展示
            this.paper.setViewBox(-dx, -dy, clientWidth, clientHeight)
        }
    }

    // 更新渲染节点和边到paper中
    updateRender () {
        const nodeList = this.getNodes(), linkList = this.getEdges();
        // 保存所有的node和edge，以便于在circle的点击事件里访问
        let paths = [], 
        setRectText = this.paper.set(); // 创建一个集合;

        let i = 0
        while (i < nodeList.length) {
            // const rect = paper.rect(nodes[i].x, nodes[i].y, nodes[i].width, nodes[i].height, 5)
            // Paint布局（例如SVG和HTML）通常使用标准的盒子模型，元素的x和y位置从左上角开始计算。
            // 然而，DagreJS对位置的计算则略有不同，其x和y是基于元素的中心点进行的。
            // 需要转换元素x和y坐标才能对齐连线
            const rect = this.paper.rect(nodeList[i].x - nodeList[i].width / 2, nodeList[i].y - nodeList[i].height / 2, nodeList[i].width, nodeList[i].height, 5);
            rect.isExpanded = nodeList[i].isExpanded
            rect.key = nodeList[i].id
            const rectBox = rect.getBBox()

            let circleMark, signMark;

            // 判断当前节点是否为父节点
            if (this.isParentNode(rect)) {
                // 标记坐标以当前节点中心点为参考，x=节点x+节点宽/2，y=节点y
                const markerCoordinateX =  nodeList[i].x + nodeList[i].width / 2, markerCoordinateY = nodeList[i].y;

                // 绘制点击圆图标
                circleMark = this.paper.circle(
                    markerCoordinateX,
                    markerCoordinateY,
                    8
                )
                // 配置circleMark的key等于当前节点，方便控制
                circleMark.key = rect.key
                // 设置circle层级最高toFront()，toBack() 
                circleMark.toFront()
                circleMark.attr({ fill: '#10ac84', stroke: 'none'})
                // 鼠标悬浮事件
                circleMark.hover(() => {
                    circleMark.attr('cursor', 'pointer')
                })

                // 绘制加号或者减号
                signMark = this.paper.path(subtraction(markerCoordinateX, markerCoordinateY))
                // 配置signMark的key等于当前节点，方便控制
                signMark.key = rect.key
                signMark.attr({ stroke: '#fff', 'stroke-width': 2 })

                signMark.click(() => {
                    // 变换当前rect展开或者折叠状态isExpanded
                    rect.isExpanded = !rect.isExpanded
                    if (rect.isExpanded) { 
                        signMark.attr('path', subtraction(markerCoordinateX, markerCoordinateY))
                        this.showAllNodeWithPath(rect, paths, Array.from(setRectText))
                    } else {
                        signMark.attr('path', addition(markerCoordinateX, markerCoordinateY))
                        this.hiddenAllNodeWithPath(rect, paths, Array.from(setRectText))
                    }
                    return false
                })
                // 点击事件
                circleMark.click((element) => {
                    // 变换当前rect展开或者折叠状态isExpanded
                    rect.isExpanded = !rect.isExpanded
                    if (rect.isExpanded) { 
                        signMark.attr('path', subtraction(markerCoordinateX, markerCoordinateY))
                        this.showAllNodeWithPath(rect, paths, Array.from(setRectText))
                    } else {
                        signMark.attr('path', addition(markerCoordinateX, markerCoordinateY))
                        this.hiddenAllNodeWithPath(rect, paths, Array.from(setRectText))
                    }
                    return false
                })
            }

            rect.attr({ stroke: Raphael.getColor(), 'stroke-width': 1 })
            const { cx, cy, x:Rx, y:Ry, width, height }  = rectBox
            let textX = Rx + width / 2, textY = Ry + height / 2;
            const text = this.paper.text(textX, textY, nodeList[i].label)
            text.key = rect.key
            text.attr({ 'font-size': 18, 'font-weight': '600' })

            // 将rect,text,circle,mark 存进setRectText数组里，以便在circle的点击事件里访问
            setRectText.push(rect, text, circleMark, signMark)

            i++;
        }

        // 获取连接线数据
        const newLinks = linkConnector(this.config.edge.connector, linkList)
        let j = 0;

        while (j < newLinks.length) {
            const path = this.paper.path(newLinks[j])
                path.attr({ stroke: this.config.edge.fill || Raphael.getColor(), 'stroke-width': 3 })
                // 设置线的层级最低，避免遮挡点击的图标
                path.toBack()
            // 添加新建path对象属性targetId和sourceId，方便控制显示和隐藏
            path.targetId = linkList[j].targetId
            path.sourceId = linkList[j].sourceId
            // 将path存进paths数组里，以便在circle的点击事件里访问
            paths.push(path);

            j++;
        }

        // svg内容太多时会发生遮挡，影响svg-pan-zoom插件内容居中属性
        // this.resetViewBox()
    }

    // 重新计算布局坐标，如果切换布局方式
    updateLayout () {
        // 此处不能这样配置，这样配置会覆盖默认值，需要依据this.config参数进行
        // this.graph.setGraph({rankdir: this.config.rankdir })
        // 让dagre为这些节点和边做布局
        // layout图形将使用布局信息进行更新，节点node获得节点中心坐标x和y
        // 边edge获取一个points属性，该属性包括边的控制点坐标以及边与节点相交处的点
        // 边edge中此弯曲中心的x坐标和y坐标
        dagre.layout(this.graph)
    }
    getNodes () {
        let coordinateNodes = [], this_ = this;
        this.graph.nodes().forEach(function(v) {
            coordinateNodes.push(this_.graph.node(v))
            // console.log("Node " + v + ": " + JSON.stringify(this_.graph.node(v)));
        });
        return coordinateNodes
    }
    getEdges () {
        let coordinateEdges = [], this_ = this;
        this.graph.edges().forEach(function(e) {
            coordinateEdges.push(this_.graph.edge(e))
            // console.log("Edge " + e.v + " -> " + e.w + ": " + JSON.stringify(this_.graph.edge(e)));
        });
        return coordinateEdges
    }

    isParentNode (rect) {
        const linkList = this.getEdges()
        // 只要边的targetId值等于当前节点的key(也是节点的id)，就认为是父节点
        // console.log(rect, linkList);
        return linkList.some(el => el.targetId === rect.key)
    }

    showAllNodeWithPath (rect = {}, paths = [], setRectText = []) {
        for(let path of paths){
            // 查找当前点击circle对应的rect元素，在路径中对应目标id的是当前节点子节点，
            // 对应源id的是当前节点父元素
            if(path['targetId'] === rect.key) {
                let targetId = path['sourceId'];
                // 查找父节点是当前节点的所有节点
                let targetNodes = setRectText.filter(r => r.key === targetId);
                targetNodes.forEach(el => {
                    el.animate({ opacity: 1, transform: "s1" }, 300)
                    el.show();
                    path.animate({ opacity: 1, transform: "s1" }, 500) 
                    path.show();
                    // 递归处理已经隐藏的子节点的子节点
                    this.showAllNodeWithPath(el, paths, setRectText); 
                });
            }
        }
    }

    hiddenAllNodeWithPath (rect = {}, paths = [], setRectText = []) {
        for(let path of paths){
            // 查找当前点击circle对应的rect元素，在路径中对应目标id的是当前节点子节点，
            // 对应源id的是当前节点父元素
            if(path['targetId'] === rect.key) {
                let targetId = path['sourceId'];
                // 查找父节点是当前节点的所有节点
                let targetNodes = setRectText.filter(r => r.key === targetId);
                targetNodes.forEach(el => {
                    el.animate({ opacity: 0, transform: "s0.2" }, 300)
                    // 隐藏子节点
                    el.hide();
                    // 隐藏线段
                    path.animate({ opacity: 0, transform: "s0.2" }, 500) 
                    path.hide();
                    // 递归处理已经隐藏的子节点的子节点
                    this.hiddenAllNodeWithPath(el, paths, setRectText); 
                });
            }
        }
    }

    destroy () {
        this.panZoomTiger.destroy()
        this.paper.clear()
        this.paper.remove()
        this.paper = null
    }
}