import { fabric } from "fabric"
import { v4 as uuidV4 } from 'uuid'
import { regularPolygonPoints, linePoints } from './index'
import { config } from "../config/editor"

export const createShape = (selectType = "Rect", canvas, params) => {
    if (!canvas) return 

    // 每次创建都需要构建唯一ID
    const options = Object.assign({ ...config.defParams, ...params }, { id: uuidV4() })
    let shape = null;

    switch (selectType) {
        case 'Triangle':
            shape = addTriangle(options)
            // 关闭画笔
            canvas.isDrawingMode = false
            break;
        case 'Rect':
            shape = addRect(options)
            canvas.isDrawingMode = false
            break;
        case 'Polygon':
            shape = addPolygon(options)
            canvas.isDrawingMode = false
            break;
        case 'Line':
            shape = addLine(options)
            canvas.isDrawingMode = false
            break;
        case 'Circle':
            shape = addCircle(options)
            canvas.isDrawingMode = false
            break;
        case 'Ellipse':
            shape = addEllipse(options)
            canvas.isDrawingMode = false
            break;
        case 'PencilBrush':
            // 开启画笔
            // canvas.isDrawingMode = true
            // const pencilBrush = new fabric.PencilBrush(canvas)
            // pencilBrush.color = options.fill || '#333'
            // pencilBrush.width = options.size || 3
            // canvas.freeDrawingBrush = pencilBrush
            // canvas.renderAll()
            break;
        case 'Arrow':
            shape = addArrow(options)
            canvas.isDrawingMode = false
            break;
        case 'IText':
            shape = addIText(options)
            // shape.set({ width: 200 })
            // shape.enterEditing()
            canvas.isDrawingMode = false
            break;
        case 'Textbox':
            shape = addTextBox(options)
            canvas.isDrawingMode = false
            break;
        default: 
            break;
    }
    return shape
}

export const addIText = (opt) => {
    const params = {
        left: 50, 
        top: 50,
        fill: '#333333',
        fontSize: 30,
        hasRotatingPoint: false,
        centerTransform: true,
        lockUniScaling: true,
        selectable: true,
    }
    return new fabric.IText('请输入文本', params)
}

export const addTextBox = (opt) => {
    const params = {
        id: uuidV4(),
        left: 50, 
        top: 50,
        fill: '#333333',
        fontSize: 30,
        splitByGrapheme: true,
        with: 200
    }
    return new fabric.Textbox('请输入内容', params)
}

export const addLine = (opt) => {
    const params = {...opt, left: 50, top: 50}
    return new fabric.Line(linePoints(100, 200), params)
}

export const addTriangle = (opt) => {
    const params = {...opt, width: 100, height: 100, left: 50, top: 50, fill: "#CDE8E5" }
    return new fabric.Triangle(params)
}

export const addRect = (opt) => {
    const params = {...opt, width: 180, height: 180, left: 50, top: 50 }
    return new fabric.Rect(params)
}

export const addPolygon = (opt) => {
    const params = {...opt, left: 50, top: 50, fill: "#FFE6E6" }
    return new fabric.Polygon(regularPolygonPoints(6, 50), params)
}

export const addCircle = (opt) => {
    const params = {...opt, radius: 50, left: 50, top: 50, rx: 60, ry: 30, fill: "#68D2E8" }
    return new fabric.Circle(params)
}

export const addEllipse = (opt) => {
    const params = {...opt, radius: 50, left: 50, top: 60, rx: 60, ry: 30, fill: "#7AB2B2" }
    return new fabric.Ellipse(params)
}

export const addArrow = (opt) => {
    const triangle = new fabric.Triangle({
        width: 9,
        height: 15,
        fill: '#272727',
        left: 150,
        top: 137,
        angle: 90
    });

    const line = new fabric.Rect({
        left: 100,
        top: 140,
        width: 40,
        height: 3,
        fill: '#272727',
        originX: 'left',
        originY: 'top',
        centeredRotation: true
    });
    
    return new fabric.Group([line, triangle], {
        id: Date.now(),
        left: 100,
        top: 100,
        angle: 0
    });
}