/**
 * @author qiuny
 * @description 跨组件图层属性，解决跨组件无法及时获取属性问题
 * @class
 * @see {@link CoverEditor}
 */


class AttributesPlugin {
    static apis = ['setLayerAttributes']
    static events = ["onAttributesChange"];
    constructor(canvas, editor) {
        this.canvas = canvas
        this.editor = editor
        this.attributes = {
            strokeWidth: 2,
            fill: 'black',
            used: '',
            strokeLineCap: 'round',
            strokeLineJoin: 'round',
            strokeDashDistance: 0,
            strokeDashOffset: undefined,
            strokeDashArray: []
        }
    }

    setLayerAttributes (opt = { strokeWidth: 0, fill: 'none'}) {
        this.attributes = Object.assign(this.attributes, {...opt })
        this.onAttributesChange()
    }

    // 广播事件，所有监听该事件的页面都会收到通知
    onAttributesChange () {
        this.editor.emit('onAttributesChange', this.attributes)
    }
}

export default AttributesPlugin