/**
 * @author qiuny
 * @description 分组文本含有文本和其他图层
 * @class
 * @see {@link CoverEditor}
 */
import { fabric } from "fabric";
import { v4 as uuidV4 } from "uuid";

class GroupTextPlugin {
    // static apis = ['setCompositeGroup']
    // static events = ["onAttributesChange"];
    constructor(canvas, editor) {
        this.canvas = canvas
        this.editor = editor
        this.isMouseDown = false
        this.groupData = null
        this._init()
    }

    _init() {
        this.canvas.on('mouse:down', (opt) => {
            this.isMouseDown = true;
            // 重置选中controls
            if (
                opt.target &&
                !opt.target.lockMovementX &&
                !opt.target.lockMovementY &&
                !opt.target.lockRotation &&
                !opt.target.lockScalingX &&
                !opt.target.lockScalingY
            ) {
                opt.target.hasControls = true;
            }
        });

        this.canvas.on('mouse:up', () => {
            this.isMouseDown = false;
        });

        // 双击组内文本框
        this.canvas.on('mouse:dblclick', (opt) => {
            const { target } = opt
            if (target && target.type === 'group') {
                const selectedObject = this._getGroupObj(opt)
                if (!selectedObject) return;
                selectedObject.selectable = true
                // 由于组内的元素，双击以后会导致controls偏移，因此隐藏他
                if (selectedObject.hasControls) {
                    selectedObject.hasControls = false;
                }
                if (this.isText(selectedObject)) {
                    this._bindTextEditingEvent(selectedObject, opt);
                    return;
                }
                this.canvas.setActiveObject(selectedObject);
                this.canvas.renderAll();
            }
        })
    }

    _bindTextEditingEvent (textObject, opt) {
        let tempText;
        const groupMatrix = opt.target.calcTransformMatrix();
        const a = groupMatrix[0];
        const b = groupMatrix[1];
        const c = groupMatrix[2];
        const d = groupMatrix[3];
        const e = groupMatrix[4];
        const f = groupMatrix[5];
        // 通过点击的text文本坐标，转换成分组内坐标，避免新增文本错位显示
        const newX = a * textObject.left + c * textObject.top + e;
        const newY = b * textObject.left + d * textObject.top + f;

        textObject.visible = false
        textObject.clone(newText => {
            tempText = newText
            tempText.set({
                id: uuidV4(),
                styles: textObject.styles,
                visible: true,
                left: newX,
                top: newY,
                fill: textObject.fill,
                angle: textObject.angle,
                scaleY: textObject.scaleY,
                scaleX: textObject.scaleX,
                selectable: true,
                hasControls: false, // 防止拖动文本后位置偏移，暂时关闭
                editable: true
            })

            tempText.on('editing:exited', function () {
                console.log('editing:exited');
                textObject.text = tempText.text
                textObject.visible = true
                tempText.visible = false
                this.canvas.remove(tempText)
                opt.target.addWithUpdate()
            })
            this.canvas.add(tempText)
            this.canvas.setActiveObject(tempText);
        })

        // 更新分组和画布
        opt.target.addWithUpdate()
        this.canvas.renderAll()
    }

    _getGroupObj(opt) {
        const pointer = this.canvas.getPointer(opt.e, true);
        // 用于在对象内部搜索包含边界框中的指针或绘制时包含pointerOnCanvas的对象的函数
        // _searchPossibleTargets是fabric内部函数
        const clickObj = this.canvas._searchPossibleTargets(opt.target?._objects, pointer);
        return clickObj;
    }

    isText(obj) {
        return obj.type && ['i-text', 'text', 'textbox'].includes(obj.type);
    }

    destroy() {
        console.log('GroupTextPlugin destroy');
    }
}

export default GroupTextPlugin