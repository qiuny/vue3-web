import { fabric } from 'fabric'

// 点类
export const Point = fabric.util.createClass({
    initialize: function (x, y) {
        this.x = x || 0;
        this.y = y || 0;
    },
    toString: function () {
        return this.x + '/' + this.y;
    }
})


// 彩色点子类
export const ColoredPoint = fabric.util.createClass(Point, {
    initialize: function (x, y, color) {
        this.callSuper('initialize', x, y);
        this.color = color || '#000';
    },
    toString: function () {
        return this.callSuper('toString') + ' (color: ' + this.color + ')';
    }
});

// 矩形文本类
fabric.LabeledRect = fabric.util.createClass(fabric.Rect, {
    type: 'LabeledRect',
    initialize: function (options) {
        options || (options = {})
        // 传递参数options
        this.callSuper('initialize', options)
        // 配置宽高
        this.set({
            widht: 100,
            height: 50
        })
        // 配置标签label
        this.set('label', options.label || '')

    },
    toObject: function () {
        return fabric.util.object.extend(this.callSuper('toObject'), {
            label: this.get('label')
        })
    },
    fromObject: function (object, callback) {
        return fabric.util.object._fromObject('LabeledRect', object, callback)
    },
    _render: function (ctx) {
        // 渲染内容
        this.callSuper('_render', ctx)
        // ctx.font = '20px Helvetica'
        // ctx.fillStyle = "#10ac84"
        // 使标签的字体和填充值可配置
        ctx.font = this.labelFont;
        ctx.fillStyle = this.labelFill;
        ctx.fillText(this.label, -this.width / 2, -this.height / 2 + 20)
    }
})

// 这样添加的方法会挂载到fabric对象的子类属性
fabric.LabeledRect.fromObject = function (object, callback) {
    return fabric.Object._fromObject('LabeledRect', object, callback, 'aProp');
}

export default fabric.LabeledRect

