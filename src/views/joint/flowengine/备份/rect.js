const rect = new shapes.standard.Rectangle({
    type: 'FlowNodeWrap',
    position: {
        x: 80,
        y: 50,
    },
    size: { width: 180, height: 100 },
    attrs: {
        body: { fill: "#3B3561", rx: 6, stroke: 'none' },
        label: {
            text: '发起人自选  >',
            textAnchor: 'middle',
            refX: '0%',
            refY: '0%',
            xAlignment: 40, // 水平距离
            fill: '#fff'
        },
        description: {
            text: 'description',
            textAnchor: 'buttom',
            refX: '0%',
            refY: '0%',
            xAlignment: 40, // 水平距离
            fill: '#fff'
        }
    },
    ports: {
        groups: {
            in: portsIn,
            out: portsOut
        }
    }
})
rect.addPorts([
    {
        group: 'in',
        attrs: { label: { text: 'part 1'}, class: 'custom-cursor'},
        args: { x: '0%', y: '40%' }
    },
    {
        group: 'in',
        attrs: { label: { text: 'part 2'}}
    },
])

rect.addTo(graph.value)

const { position, size } = rect.attributes
const rect2 = new shapes.standard.Rectangle({
    type: 'FlowNodeTitle',
    position: {
        x: position.x,
        y: position.y,
    },
    size: { width: size.width, height: 24},
    attrs: {
        body: { fill: "#FFDD4A", ry: 6, rx: 1, stroke: 'none' },
        label: {
            text: '普通节点',
            textAnchor: 'middle'
        }
    },
})

// 这样添加rect2元素在rect的分组之外
rect.graph.addCell(rect2)
// rect.graph.addCells([rect2])
// 绑定元素
// rect.embed(rect2)
// 解除绑定元素
// rect.unembed(rect2)
// 子元素移动，父元素也跟着移动
rect2.on("change:position", function(evt) {
    const { position, size } = rect2.attributes
    rect.set('position', { x: position.x, y: position.y})
})