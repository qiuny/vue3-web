import { dia, shapes, layout } from "@joint/core"
import { portsIn, portsOut } from './port'

export const customRectangle = (opt = {}) => {
    const customRect = dia.Element.define('CustomRectangle', 
    {
        size: { width: 100, height: 180 },
        type: "CustomRectangle",
        attrs: {
            header: {
                refWidth: '100%',
                y: 0,
                height: 30,
                strokeWidth: 1,
                stroke: "#666",
                // The color must match to one of the colors in the COLORS array
                // in order to make the color picker work.
                fill: "#2C3E50",
                rx: 6,
                ry: 6
            },
            headerLabel: {
                textVerticalAnchor: 'middle',
                textAnchor: 'middle',
                refX: '50%',
                refY: 15, // Half of header's height
                fontSize: 14,
                fill: '#333333'
            },
            body: {
                rx: 10, // add a corner radius
                ry: 10,
                refWidth: '100%',
                refHeight: '100%',
                strokeWidth: 1,
                stroke: '#000000',
                fill: '#FFFFFF'
            },
            label: {
                textVerticalAnchor: 'middle',
                textAnchor: 'middle',
                refX: '50%',
                refY: '30%',
                fontSize: 16,
                fill: '#333333',
                fontWeight: 600
            },
            description: {
                textAnchor: 'middle', // align text to left
                refX: '50%',
                refY: '50%',
                fill: '#352641',
                fontSize: 14,
                fontWeight: 400,
                opacity: 0.5,
            },
            rigthArrow: {
                fill: 'none',
                stroke: 'tomato',
                strokeWidth: 4,
                d: 'M-6,0 L0,6 L-6,12', 
                refX: '90%',
                refY: '30%',
                style: {
                    cursor: 'pointer'
                }
            }
        },
        ports: {
            groups: {
                in: portsIn,
                out: portsOut
            }
        },
        designMode: false,
        inputs: [],
        outputs: [],
    },
    {
        markup: [
            {
                tagName: 'rect',
                selector: 'body',
            },
            {
                tagName: 'rect',
                selector: 'header',
            },
            {
                tagName: 'text',
                selector: 'headerLabel',
            },  
            {
                tagName: 'text',
                selector: 'label'
            },
            {
                tagName: 'text',
                selector: 'description'
            },
            {
                tagName: 'path',
                selector: 'rigthArrow'
            },
        ],
        initialize() {
            // 这里初始化自定义组件，监听模型属性的变化，并执行相应的更新逻辑
            dia.Element.prototype.initialize.apply(this, arguments);
            // console.log(this.getPorts(), this.get('size'), 666);
            if (this.get("designMode") || this.getPorts().length === 0) {
                // If the element is in the design mode, we are ok to re-build
                // the `ports` and lost all user's values.
                // this.buildPortItems();
            }
            // this.on(
            //     "change:inputs change:outputs change:designMode change:fieldWidth",
            //     (el, changed, opt) => this.buildPortItems(opt)
            // );
        },
    })

    // 将customRect形状添加到shapes中
    shapes.standard.CustomRectangle = customRect

    // 捕获输入更改并更新模型的视图
    const ShapeView = dia.ElementView.extend({
        events: {
            change: "onInputChange"
        },
        onInputChange: function (evt) {
            // const fieldEl = evt.target;
            // const id = this.findAttribute("port", fieldEl);
            // this.model.portProp(id, "attrs/field/props/value", fieldEl.value);
        }
    })

    return customRect
}