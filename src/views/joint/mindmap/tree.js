import { v4 as uuidV4 } from "uuid";

class Node {
    /**
     * @description 节点对象
     * @param { String } label 节点名称
     * @param { Number } parentId 父级id
     */
    constructor({ label = '节点名称', parentId = '', children = []}) {
        this.id = uuidV4();
        this.label = label;
        this.parentId = parentId;
        this.children = children || []
    }
}

export const basicTree = {
    id: uuidV4(),
    label: '前端知识',
    children: [
        {
            id: uuidV4(),
            label: 'HTML',
            children: []
        },
        {
            id: uuidV4(),
            label: 'JavaScript',
            children: []
        },
        {
            id: uuidV4(),
            label: 'CSS',
            children: [
                {
                    id: uuidV4(),
                    label: 'CSS1',
                    children: []
                },
                {
                    id: uuidV4(),
                    label: 'CSS2',
                    children: []
                }
            ]
        },
        {
            id: uuidV4(),
            label: 'TypeScript',
            children: []
        }
    ]
}