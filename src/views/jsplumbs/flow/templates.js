/**
 * jsplump 节点模板配置中心
 */
import { jsPlumbUtil } from 'jsplumb';
export const list = [
    { name: "普通节点", color: '#10AC84', icon: 'md-checkmark-circle', key: jsPlumbUtil.uuid() },
    { name: "判断条件", color: '#00425A', icon: 'md-git-commit', key: jsPlumbUtil.uuid() },
    { name: "流程开始", color: '#BFDB38', icon: 'md-play', key: jsPlumbUtil.uuid() },
    { name: "流程结束", color: '#FC7300', icon: 'ios-power', key: jsPlumbUtil.uuid() },
    { name: "分支条件", color: '#1F8A70', icon: 'md-git-branch', key: jsPlumbUtil.uuid() },
    { name: "审批人", color: '#3C79F5', icon: 'md-contact', key: jsPlumbUtil.uuid() },
    { name: "抄送人", color: '#EB6440', icon: 'ios-contacts', key: jsPlumbUtil.uuid() }
]