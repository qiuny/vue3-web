/**
 * jsPlumb通用参数配置
 */
export const plumbConfig = () => {
    return {
        isSource: true, // 源节点
        isTarget: true, // 目标节点
        connector: ["Bezier"], // 贝塞尔曲线
        endpoint: ["Dot", { radius: 4, cssClass: "circle", hoverClass: "circle1" }], // 端点类型：圆点
        connectorStyle: { stroke: "rgb(68, 85, 102)", fill: "none", strokeWidth: 1 }, // 连线样式
        connectorHoverStyle: { strokeWidth: 3 }, // 连接线激活样式
        maxConnections: -1, // 端点默认只能有一条线，可以配置-1表示无限条连接线
        connectorOverlays: [["Arrow", { width: 20, length: 20, location: 1 }]], // 配置箭头样式
        paintStyle: {
            // 端点样式
            fill: "rgb(68, 85, 102)",
            strokeWidth: 4,
        },
        ConnectionsDetachable: false, // 鼠标不能拖拽删除线
        hoverPaintStyle: {
            // 端点鼠标悬浮样式
            fill: "rgb(68, 85, 102)",
        },
    };
};

/**
 * jsplumb 初始化配置
 */
export const defaultConfig = {
    setting: {
        // 动态锚点位置
        Anchors: [
            "Bottom",
            "BottomCenter",
            "BottomLeft",
            "BottomRight",
            "Center",
            "Left",
            "LeftMiddle",
            "Right",
            "RightMiddle",
            "Top",
            "TopCenter",
            "TopLeft",
            "TopRight",
        ],
        // 容器id
        Container: 'jsplumbEditorId',
        // 连接线样式
        Connector: ['Bezier', { curviness: 100 }],
        // 鼠标不能拖动删除线
        ConnectionsDetachable: false,
        // 删除线的时候节点不删除
        DeleteEndpointsOnDetach: false,
        // 连线的两端端点类型：圆形
        // Endpoint: ['Dot', {radius: 5, cssClass: 'custom-dot', hoverClass: 'custom-dot-hover'}],
        // 空白端点
        Endpoint: ['Blank', {Overlays: ''}],
        // 连线的两端端点样式
        EndpointStyle: { fill: '#1879ffa1', outlineWidth: 1 },
        // 连线样式
        PaintStyle: {
            stroke: '#10AC84',
            fill: 'none',
            strokeWidth: 2
        },
        // 拖拽配置
        DragOptions: { cursor: 'pointer', zIndex: 2000 },
        ConnectionOverlays: [
            // 箭头叠加
            ['Arrow', {
                width: 10, // 箭头尾部的宽度
                length: 8, // 从箭头的尾部到头部的距离
                location: 1, // 位置，建议使用0～1之间
                direction: 1, // 方向，默认值为1（表示向前），可选-1（表示向后）
                foldback: 0.623 // 折回，也就是尾翼的角度，默认0.623，当为1时，为正三角
            }],
            ['Label', {
                label: '',
                location: 0.1,
                cssClass: 'aLabel'
            }] 
        ],
        // 鼠标滑过线的样式
        HoverPaintStyle: { stroke: 'tomato', strokeWidth: 3 }
    },
    // 连接线参数
    connectOptions: {
        isSource: true,
        isTarget: true,
        // 动态锚点、提供了4个方向 Continuous、AutoDefault
        anchor: 'Continuous',
        // 设置连线上面的label样式
        labelStyle: {
            cssClass: 'flowLabel'
        },
    },
    sourceOptions: {
        // 设置可以拖拽的类名，只要鼠标移动到该类名上的DOM，就可以拖拽连线
        filter: '.flow-node-drag',
        filterExclude: false,
        anchor: 'Continuous',
        // 是否允许自己连接自己
        allowLoopback: true,
        maxConnections: -1,
        onMaxConnections: function (info, e) {
            console.log(`超过了最大值连线: ${info.maxConnections}`)
        }
    },
    // 目标节点参数配置
    targetOptions: {
        // 设置可以拖拽的类名，只要鼠标移动到该类名上的DOM，就可以拖拽连线
        filter: '.flow-node-drag',
        filterExclude: false,
        // 是否允许自己连接自己
        anchor: 'Continuous',
        allowLoopback: true,
        dropOptions: { hoverClass: 'ef-drop-hover' }
    }
};