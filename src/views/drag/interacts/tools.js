import { reactive } from 'vue'
import interact from 'interactjs'

export const useElementMoveOrResizable = (className = '', enabled = true) => {
    const coordinate = reactive({
        origin: {
            x: 0,
            y: 0
        },
        to: {
            x: 0,
            y: 0
        }
    })
    const rectResizable = reactive({
        width: `0px`,
        height: `0px`,
        // transform: `translate(0px, 0px)`
    })
    interact(className).draggable({
        enabled: enabled,
        // startAxis: 'xy', // 固定只能延x轴或y轴拖拽元素
        // lockAxis: 'start',
        listeners: {
            start (event) {
                coordinate.origin.x = coordinate.to.x
                coordinate.origin.y = coordinate.to.y
            },
            move (event) {
                // coordinate.to.x += event.dx
                // coordinate.to.y += event.dy
                const { target } = event

                coordinate.to.x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
                coordinate.to.y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy

                event.target.style.transform = `translate(${coordinate.to.x}px, ${coordinate.to.y}px)`

                target.setAttribute('data-x', coordinate.to.x)
                target.setAttribute('data-y', coordinate.to.y)
            },
            end (event) {
                const { target } = event
                // Reset the attributes after dragging ends
                // target.setAttribute('data-x', 0);
                // target.setAttribute('data-y', 0);
            }
        }
    })
    .resizable({
        edges: {
            top: true,
            left: true,
            bottom: true,
            right: true,
        },
        listeners: {
            move: function (event) {
                let { x, y } = event.target.dataset
                x = (parseFloat(x) || 0) + event.deltaRect.left
                y = (parseFloat(y) || 0) + event.deltaRect.top

                rectResizable.width = `${event.rect.width}px`
                rectResizable.height = `${event.rect.height}px`
                // rectResizable.transform = `translate(${x}px, ${y}px)`
                Object.assign(event.target.style, { ...rectResizable })

                Object.assign(event.target.dataset, { x, y })
            }
        }
    })

    return {
        ...coordinate,
        ...rectResizable
    }
}


export const useElementMoveCoordinate = (className = '', enabled = true) => {
    const coordinate = reactive({
        origin: {
            x: 0,
            y: 0
        },
        to: {
            x: 0,
            y: 0
        }
    })
    interact(className).draggable({
        enabled: enabled,
        listeners: {
            start (event) {
                coordinate.origin.x = coordinate.to.x
                coordinate.origin.y = coordinate.to.y
                console.log( coordinate.origin);
            },
            move (event) {
                coordinate.to.x += event.dx
                coordinate.to.y += event.dy
                event.target.style.transform = `translate(${coordinate.to.x}px, ${coordinate.to.y}px)`
            }
        }
    })

    return coordinate
}

export const useElementResizable = (className = '') => {
    const rectResizable = reactive({
        width: `0px`,
        height: `0px`,
        transform: `translate(0px, 0px)`
    })
    interact(className).resizable({
        edges: {
            top: true,
            left: true,
            bottom: true,
            right: true,
        },
        listeners: {
            move: function (event) {
                let { x, y } = event.target.dataset
                x = (parseFloat(x) || 0) + event.deltaRect.left
                y = (parseFloat(y) || 0) + event.deltaRect.top

                rectResizable.width = `${event.rect.width}px`
                rectResizable.height = `${event.rect.height}px`
                rectResizable.transform = `translate(${x}px, ${y}px)`
                Object.assign(event.target.style, { ...rectResizable })

                Object.assign(event.target.dataset, { x, y })
            }
        }
    })

    return rectResizable
}