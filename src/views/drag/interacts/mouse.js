import { ref } from 'vue'
import { useEventListener } from './events'

export function useMouseEvent(className, type = 'mousedown') {
    const findElement = document.querySelector(className)
    console.log(findElement, className,444);
    if (!findElement) return null
    const mouseType = ref(type)
    useEventListener(findElement, type, (event) => {
        mouseType.value = event.type
        console.log(event.type, 7777);
    })
    return mouseType
}