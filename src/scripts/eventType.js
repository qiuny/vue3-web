// 选择模式
export const SelectMode = {
    EMPTY: '',
    ONE: 'one',
    MULTI: 'multiple',
}

// 选择事件（用于广播）
export const SelectEvent = {
    ONE: 'selectOne',
    MULTI: 'selectMultiple',
    CANCEL: 'selectCancel',
}

export default { SelectMode, SelectEvent };