/**
 * 项目名配置
 */
export const projectName = 'bruce'

export const projectNameCn = 'Bruce博客'

/**
 * 是否开启动态菜单
 * true 开启
 * false 关闭
 */ 
export const isEnableDynamicRouter = false