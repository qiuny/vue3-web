import { useRouter, useRoute, RouterLink } from 'vue-router'

/**
 * 封装路由，导出模块
 */
export const customRoute = () => {
    const router = useRouter()
    const route  = useRoute()
    return {
        route,
        router,
        RouterLink
    }
}
