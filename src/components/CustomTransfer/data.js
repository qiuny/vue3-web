import { v4 as uuidV4 } from 'uuid'

export const data = [
    {
        name: 'John Brown',
        age: 18,
        address: 'New York No. 1 Lake Park',
        date: '2016-10-03',
        id: uuidV4()
    },
    {
        name: 'Jim Green',
        age: 24,
        address: 'London No. 1 Lake Park',
        date: '2016-10-01',
        id: uuidV4()
    },
    {
        name: 'Joe Black',
        age: 30,
        address: 'Sydney No. 1 Lake Park',
        date: '2016-10-02',
        id: uuidV4()
    },
    {
        name: 'Jon Snow',
        age: 26,
        address: 'Ottawa No. 2 Lake Park',
        date: '2016-10-04',
        id: uuidV4()
    }
]

export const column = [
    {
        type: 'selection',
        width: 60,
        align: 'center'
    },
    {
        title: '姓名',
        key: 'name'
    },
    {
        title: '年龄',
        key: 'age'
    },
    {
        title: '地址',
        key: 'address'
    }
]