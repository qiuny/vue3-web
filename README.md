# vue3-web 
#### ✨技术栈
| 名称  | 版本  | 描述   |
|---|---|---|
| vue  |  3.2.47 | vue核心包  |
| vue-router | 4.2.2 | vue路由4  |
| vite | 5.0.3 | vue打包工具  |
| view-ui-plus| 1.3.14  | iview UI库 |
| uuid  | 9.0.0  | 生成唯一id |

#### 👓介绍
    分享前端开发知识，帮助更多小伙伴解决vue的问题

#### 案例源码
##### 1、[iview+vue3穿梭实现多列源码](https://gitee.com/qiuaiyun/vue3-custom-component/tree/master/vue3-vite-iview/src/components/CustomTransfer)

##### 2、[flip动画+vue3图片预览器源码](https://gitee.com/qiuaiyun/vue3-custom-component/tree/master/vue3-vite-iview/src/components/flip/ImageViewer)

